\section{Meditation-Related Adverse Effects}
\label{sec:meditation-related-adverse-effects}

\emph{Mindfulness-based programs} have emerged as a promising treatment for a
range of conditions. Its efficacy and positive effects has been studied and
proved comparable to established psychological treatments. However, very little
is known about negative or adverse effects. We'll start a discussion defining
which adverse effects can be associated with \emph{Mindfulness}
(\Cref{subsec:adverse-effects}) and then we'll present some results of measured
adverse effects in Mindfulness-based programs
(\Cref{subsec:measuring-adverse-effects}).

\subsection{Adverse Effects}
\label{subsec:adverse-effects}

\citetitle{Britton2019} \cite{Britton2019} proposes that Mindfulness-based
programs, like other treatments, follow a non-monotonic dose-wellbeing curve.
Which is characterized by the inverted U-shaped curve shown in
\cref{fig:invertedUshape}. Which, in short, means that wellbeing can benefit
from Mindfulness processes but practitioners can suffer from \textbf{overdose}.
In the case of \emph{interoception} (body scan, breath awareness, etc.),
moderate practice leads to better body awareness and sensations while high
practice can also increase arousal, cause depression, anxiety and clinical pain
syndromes. Similarly, practicing \emph{decentering} can lead to an objective
view of oneself, stepping back from own thoughts and emotions. But if
overdosed, practitioners can suffer from dissociation, depersonalization and
out-of-body experiences.

\simplefigure[.55]{invertedUshape}{Illustrates an inverted U-shape relationship
between mindfulness-related processes (MRP) (horizontal axis) and wellbeing (vertical
axis). Panel 1 shows how low levels or deficiencies in MRPs correspond with low
levels of wellbeing. It also shows how wellbeing increases as the deficiency in
MRPs is reversed. Panel 2 illustrates how optimal levels of MRP corresponds to
maximum levels of wellbeing. Panel 3 depicts how an excess of an MRP, past what
is optimal, corresponds to a reduction in wellbeing.}

In some processes the optimal practice is difficult to define, as increased
practice can lead to both positive and negative effects at the same time. For
example, \emph{emotion regulation} helps to improve emotion control,
suppressing anxiety, depression and the emotional reactivity. Increased
practice can further attenuate negative emotions, at the same time that it can
cause emotional blunting in emotions considered positive.

Positive and negative effects are also biased from the point of view of
individuals, which makes the optimal practice point impossible to define
objectively. In the case of \emph{exposure therapy}, where practitioners
face difficult emotions, depending on the participant type we can have
beneficial, ineffective or even harmful effects. People who tend to escape from
difficult emotions can learn to accept them better. But people who tends to
immerse in them can suffer from traumatic flashbacks.

We know that adverse effects are to be expected in any treatment, but why are
they often not shown in studies? There are several sources of bias that can
lead to neglect them: \textbf{Provider bias}, researchers tend to fall in the
fallacy that some worsening is to be expected and is a positive sign that the
therapy is working. There might also be funds that depend on the outcome of the
research; \textbf{Participant bias}, research participants and psychotherapy
clients are unlikely to spontaneously report mild negative treatment reactions.
\textbf{Sample limitations}, data might be too short term for some negative
effects to be relevant or it might come from a sample of long-term participants
where we find survivorship bias.

\subsection{Measuring Adverse Effects}
\label{subsec:measuring-adverse-effects}

There are, however, studies that focus on measuring the adverse effects as a
way to prove the effectiveness of the treatment instead on focusing only on the
positive effects. \citetitle{Britton2021} \cite{Britton2021} shows the result
of a 4 year long study in which 104 middle-aged Americans with mild to severe
levels of depression have regular meditation training. Adverse effects are
measured actively, in a systematic way carried out by an independent party.
Interviews have a category-specific questionnaire apart from the common
open-ended questions. The study results are shown in
\cref{fig:adverse-effects-results}. 266 side effects where identified, the
open-ended questions only identified 32.5\% of them, proving the inefficiency
of passive monitoring.

\simplefigure[.55]{adverse-effects-results.png}{Frequencies of 
meditation-related side effects (MRSEs) and meditation-related adverse effects
(MRAEs). The gray bar indicates the percentage of the sample that reported the
presence of any MRSE. MRAEs are displayed in three tiers: MRAEs with negative
valence (blue), with negative impact (orange), and lasting bad effects, or
MRAEs with negative impacts lasting ore than 1 month (dark green), more than 1
week (green), or more than 1 day (light green).}

Insights from this study show a dose-response relationship where higher
practice intensity did not lead to higher frequency of negative experiences.
But higher practice frequency or duration did correlate with \emph{Lasting Bad
Effects}. The fact that 58\% of the participants experienced transient negative
impact adverse effects shows that some transient distressing should be
expected. But most importantly is that the rate of \emph{Lasting Bad Effects}
is similar to other psychological treatments.